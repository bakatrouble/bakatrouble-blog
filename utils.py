import os
import markdown2
import html
from collections import OrderedDict
from passlib.handlers.pbkdf2 import pbkdf2_sha256

from cache import client as cache

ENTRY_EXTENSIONS = {
    '.txt': lambda x: f'<pre>{html.escape(x)}</pre>',
    '.md': lambda x: markdown2.markdown(x, extras=['fenced-code-blocks', 'tables'])
}


class Blog:
    @staticmethod
    def init():
        users = {}
        # load users
        with open('passwd') as passwd:
            for crypt in passwd.readlines():
                username, crypt = crypt.split(':')
                users[username] = crypt
        cache.set('blog-users', users)
        # load entries
        entries = OrderedDict()
        for entry in reversed(os.listdir('posts')):
            post_path = os.path.join('posts', entry)
            ext = os.path.splitext(entry)[1]
            if os.path.isfile(post_path) and ext in ENTRY_EXTENSIONS.keys():
                with open(post_path, encoding='utf-8') as entry_content:
                    entries[entry] = {
                        'content': ENTRY_EXTENSIONS[ext](entry_content.read())
                    }
        cache.set('blog-entries', entries)

    @staticmethod
    def check_auth(username, password):
        users = cache.get('blog-users')
        return username in users and pbkdf2_sha256.verify(password, users[username].strip())

    @staticmethod
    def get_posts():
        return cache.get('blog-entries')

    @staticmethod
    def get_post(name):
        return Blog.get_posts()[name]

Blog.init()

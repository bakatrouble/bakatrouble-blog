import functools
import math
import os
import subprocess
from collections import OrderedDict
from itertools import islice
from bottle import Bottle, request, redirect, jinja2_view, route, run, static_file, auth_basic
from utils import Blog
from git import Repo

view = functools.partial(jinja2_view, template_lookup=['templates'])

PAGE_SIZE = 10

app = Bottle()


@app.route('/')
@view('index.html')
def index():
    try:
        page = int(request.query.page or 0)
    except ValueError:
        page = 0
    all_posts = Blog.get_posts()
    post_count = len(all_posts)
    posts = OrderedDict(islice(all_posts.items(), PAGE_SIZE * page, PAGE_SIZE * (page + 1)))
    page_count = math.ceil(post_count / PAGE_SIZE)
    return {'page': page, 'posts': posts, 'post_count': post_count, 'page_count': page_count}


@app.route('/static/<path:path>')
def callback(path):
    return static_file(path, root='static')


@app.route('/update')
@auth_basic(Blog.check_auth)
def update():
    try:
        repo = Repo(os.path.join(os.path.dirname(__file__), 'posts'))
        o = repo.remotes.origin
        o.pull()
        Blog.init()
    except subprocess.CalledProcessError:
        return 'Some error has occurred while pulling updates'
    return redirect('/')


if __name__ == '__main__':
    app.run()
